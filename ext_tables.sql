#
# Table structure for table 'tx_leafletosm_domain_model_address'
#
CREATE TABLE tx_leafletosm_domain_model_address (

	title varchar(255) DEFAULT '' NOT NULL,
	street varchar(255) DEFAULT '' NOT NULL,
	zip varchar(255) DEFAULT '' NOT NULL,
	city varchar(255) DEFAULT '' NOT NULL,
	configuration_map varchar(255) DEFAULT '' NOT NULL,
	latitude double(11,15) DEFAULT '0.000000000000000' NOT NULL,
	longitude double(11,15) DEFAULT '0.000000000000000' NOT NULL,
	address varchar(255) DEFAULT '' NOT NULL,
	marker int(11) unsigned NOT NULL default '0',
	image_size smallint(5) unsigned DEFAULT '0' NOT NULL,
	image_width int(11) DEFAULT '0' NOT NULL,
	image_height int(11) DEFAULT '0' NOT NULL,
	marker_anchor_x int(11) DEFAULT '0' NOT NULL,
	marker_anchor_y int(11) DEFAULT '0' NOT NULL,
	popup_anchor_x int(11) DEFAULT '0' NOT NULL,
	popup_anchor_y int(11) DEFAULT '0' NOT NULL,
	info_window_content text,
	info_window_images int(11) unsigned NOT NULL default '0',
	close_by_click smallint(5) unsigned DEFAULT '0' NOT NULL,
	open_by_click smallint(5) unsigned DEFAULT '0' NOT NULL,
	opened smallint(5) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_leafletosm_domain_model_map'
#
CREATE TABLE tx_leafletosm_domain_model_map (

	title varchar(255) DEFAULT '' NOT NULL,
	width varchar(255) DEFAULT '' NOT NULL,
	height varchar(255) DEFAULT '' NOT NULL,
	zoom int(11) DEFAULT '0' NOT NULL,
	show_addresses smallint(5) unsigned DEFAULT '0' NOT NULL,
	show_categories smallint(5) unsigned DEFAULT '0' NOT NULL,
	kml_url text,
	kml_preserve_viewport smallint(5) unsigned DEFAULT '0' NOT NULL,
	scroll_zoom smallint(5) unsigned DEFAULT '0' NOT NULL,
	min_zoom int(11) DEFAULT '0' NOT NULL,
	max_zoom int(11) DEFAULT '0' NOT NULL,
	center_longitude double(11,15) DEFAULT '0.000000000000000' NOT NULL,
	center_latitude double(11,15) DEFAULT '0.000000000000000' NOT NULL,
	geo_location smallint(5) unsigned DEFAULT '0' NOT NULL,
	preview_image int(11) unsigned NOT NULL default '0',
	draggable smallint(5) unsigned DEFAULT '0' NOT NULL,
	double_click_zoom smallint(5) unsigned DEFAULT '0' NOT NULL,
	marker_cluster smallint(5) unsigned DEFAULT '0' NOT NULL,
	marker_cluster_zoom int(11) DEFAULT '0' NOT NULL,
	marker_cluster_size int(11) DEFAULT '0' NOT NULL,
	marker_search smallint(5) unsigned DEFAULT '0' NOT NULL,
	default_type int(11) DEFAULT '0' NOT NULL,
	scale_control smallint(5) unsigned DEFAULT '0' NOT NULL,
	fullscreen_control smallint(5) unsigned DEFAULT '0' NOT NULL,
	zoom_control smallint(5) unsigned DEFAULT '0' NOT NULL,
	map_type_control smallint(5) unsigned DEFAULT '0' NOT NULL,
	map_types varchar(255) DEFAULT '' NOT NULL,
	show_route smallint(5) unsigned DEFAULT '0' NOT NULL,
	calc_route smallint(5) unsigned DEFAULT '0' NOT NULL,
	travel_mode varchar(255) DEFAULT '' NOT NULL,
	measure_system varchar(255) DEFAULT '' NOT NULL,
	map_addresses int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_leafletosm_domain_model_category'
#
CREATE TABLE tx_leafletosm_domain_model_category (

	losm_marker int(11) unsigned NOT NULL default '0',
	losm_image_size smallint(5) unsigned DEFAULT '0' NOT NULL,
	losm_image_width int(11) DEFAULT '0' NOT NULL,
	losm_image_height int(11) DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_leafletosm_domain_model_address'
#
CREATE TABLE tx_leafletosm_domain_model_address (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table 'tx_leafletosm_map_address_mm'
#
CREATE TABLE tx_leafletosm_map_address_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Extend table structure of table 'sys_category'
#
CREATE TABLE sys_category (
	osm_marker int(11) unsigned NOT NULL default '0',
	osm_image_size tinyint(1) unsigned DEFAULT '0' NOT NULL,
	osm_image_width int(11) DEFAULT '0' NOT NULL,
	osm_image_height int(11) DEFAULT '0' NOT NULL,
	osm_marker_anchor_x int(11) DEFAULT '0' NOT NULL,
	osm_marker_anchor_y int(11) DEFAULT '0' NOT NULL,
	osm_popup_anchor_x int(11) DEFAULT '0' NOT NULL,
	osm_popup_anchor_y int(11) DEFAULT '0' NOT NULL,
);
