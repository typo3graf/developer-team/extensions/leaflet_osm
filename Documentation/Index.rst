﻿.. include:: Includes.txt

======================
Leaflet-OpenStreetMaps
======================

.. only:: html

    :Classification:
        leaflet_osm

    :Version:
        |release|

    :Language:
        en

    :Description:
		Interactive and flexible plugin to create multiple maps in posts and pages, and to add multiple markers on those maps using Leaflet.js and OpenStreetMap.

    :Keywords:
        openstreet maps, leaflet, map, api, marker, image, style, route, kml, geolocation, categories ...

    :Copyright:
        2019

    :Author:
        Mike Tölle

    :Email:
        mtoelle@typo3graf.de

    :License:
        This document is published under the Open Content License
        available from http://www.opencontent.org/opl.shtml

    :Rendered:
        |today|


    **Table of Contents**

.. toctree::
	:maxdepth: 5
	:titlesonly:
    :glob:

    Introduction/Index
    User/Index
    Tutorial/Index
    Administrator/Index
    Configuration/Index
    KnownProblems/Index
    ToDoList/Index
    ChangeLog/Index
