﻿.. include:: ../Includes.txt
.. include:: Images.txt

.. _introduction:

Introduction
============

.. _what-it-does:

What does it do?
----------------

The Extension provides a plugin to insert a very flexible OpenStreet Map to your site.

.. _features:

Features
--------

- Written with Extbase/Fluid

- Works with jQuery

- Uses Leaflet.js and OpenStreetMap, free API key from openrouteservice.org required only for routing function

- Add your own markers via geocoding in the backend

- Customize the content of the InfoWindow via RTE

- Show a route between two markers

- Create a form in the frontend to calculate a route to a specified
  destination

- Configure the map (draggable, double click zoom...)

- Import a KML file

- Add Marker images for addresses or categories

- Marker Cluster

- Search for markers

- Create categories for the addresses and filter them via Checkboxes

- GDPR compatible - show an image preview, only if the user accept, the data from OpenStreetMap will be loaded

.. _screenshots:

Screenshots
-----------

|screenshot|

*Example of a map with one individual marker and
cluster marker, categories*

