﻿.. include:: ../Includes.txt

.. _known-problems:

Known Problems
==============

- None so far.

- If you've found some, please contact me or visit `https://gitlab.com/typo3graf/developer-team/extensions/leaflet_osm/issues>`_

- Current developer version: `https://gitlab.com/typo3graf/developer-team/extensions/leaflet_osm>`_

