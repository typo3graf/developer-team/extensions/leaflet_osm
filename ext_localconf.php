<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Typo3graf.LeafletOsm',
            'show',
            [
                'Map' => 'show'
            ],
            // non-cacheable actions
            [
                'Map' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    show {
                        iconIdentifier = leaflet_osm-plugin-osmshowmap
                        title = LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leaflet_osm_osmshowmap.name
                        description = LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leaflet_osm_osmshowmap.description
                        tt_content_defValues {
                            CType = list
                            list_type = leafletosm_show
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

			$iconRegistry->registerIcon(
				'leaflet_osm-plugin-osmshowmap',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:leaflet_osm/Resources/Public/Icons/map.svg']
			);
        // here we register "tx_leafletosm_double15"
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tce']['formevals'][\Typo3graf\LeafletOsm\Evaluation\Double15Evaluator::class] = '';
    }


);
