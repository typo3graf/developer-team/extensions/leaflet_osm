<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function() {
        $extKey = 'leaflet_osm';
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Typo3graf.LeafletOsm',
            'show',
            'OpenStreet Map'
        );
        // flexform
        $pluginSignature = str_replace('_', '', $extKey) . '_show';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'code,layout,select_key,pages,recursive';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
            $pluginSignature,
            'FILE:EXT:' . $extKey . '/Configuration/FlexForms/flexform_show.xml'
        );
    }

);
