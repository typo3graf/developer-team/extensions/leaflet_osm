<?php
defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
    'leaflet_osm',
    'tx_leafletosm_domain_model_address',
    'categories',
    [
        // Set a custom label
        'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.categories',
        // Override generic configuration, e.g. sort by title rather than by sorting
        'fieldConfiguration' => [
            'foreign_table_where' => ' AND sys_category.sys_language_uid IN (-1, 0) ORDER BY sys_category.title ASC',
        ],
        // string (keyword), see TCA reference for details
        'l10n_mode' => 'exclude',
        // list of keywords, see TCA reference for details
        'l10n_display' => 'hideDiff',
    ]
);
