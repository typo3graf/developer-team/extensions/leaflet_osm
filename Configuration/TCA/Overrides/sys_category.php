<?php
defined('TYPO3_MODE') or die();

$tempCols = [
    'osm_marker' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.marker',
        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'osm_marker',
            [
                'appearance' => [
                    'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                ],
                'maxitems' => 1,
                'overrideChildTca' => [
                    'types' => [
                        'foreign_types' => [
                            '0' => [
                                'showitem' => '
                        --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                        --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '
                        --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                        --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                        --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                        --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                'showitem' => '
                        --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                        --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '
                        --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                        --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '
                        --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                        --palette--;;filePalette'
                            ]
                        ],
                    ]
                ]
            ],
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
        ),
    ],
    'osm_image_width' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.image_width',
        'config' => [
            'type' => 'input',
            'size' => 4,
            'eval' => 'int'
        ],
    ],
    'osm_image_height' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.image_height',
        'config' => [
            'type' => 'input',
            'size' => 4,
            'eval' => 'int'
        ],
    ],
    'osm_marker_anchor_x' => [
        'exclude' => true,
        'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.marker_anchor_x',
        'config' => [
            'type' => 'input',
            'size' => 4,
            'eval' => 'int'
        ]
    ],
    'osm_marker_anchor_y' => [
        'exclude' => true,
        'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.marker_anchor_y',
        'config' => [
            'type' => 'input',
            'size' => 4,
            'eval' => 'int'
        ]
    ],
    'osm_popup_anchor_x' => [
        'exclude' => true,
        'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.popup_anchor_x',
        'config' => [
            'type' => 'input',
            'size' => 4,
            'eval' => 'int'
        ]
    ],
    'osm_popup_anchor_y' => [
        'exclude' => true,
        'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.popup_anchor_y',
        'config' => [
            'type' => 'input',
            'size' => 4,
            'eval' => 'int'
        ]
    ],
];

// add new fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $tempCols);

// new palette
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'sys_category',
    'osm_marker',
    'osm_marker, --linebreak--, osm_image_width, osm_image_height, --linebreak--, osm_marker_anchor_x, osm_marker_anchor_y, --linebreak--, osm_popup_anchor_x, osm_popup_anchor_y',
    ''
);

// add fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'sys_category',
    '--div--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:sys_category.tab.map,
	--palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.palettes.marker;osm_marker'
);
