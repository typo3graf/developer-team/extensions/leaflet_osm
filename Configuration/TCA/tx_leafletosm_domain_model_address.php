<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,street,zip,city,configuration_map,address,info_windows_content',
        'iconfile' => 'EXT:leaflet_osm/Resources/Public/Icons/marker.svg'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, street, zip, city, configuration_map, latitude, longitude, address, marker, image_width, image_height, marker_anchor_x, marker_anchor_y, popup_anchor_x, popup_anchor_y, info_windows_content, info_windows_images, close_by_click, open_by_click, opened',
    ],
    'types' => [
        '0' => [
            'showitem' => 'title,--palette--;;address,configuration_map,--palette--;;data,
					--div--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.style,
					--palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.palettes.marker;marker,
					--div--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.info_window,
					info_window_content, info_window_images, --palette--;;link,
					--palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.palettes.interaction;interaction,
					--div--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.others,
					sys_language_uid, l10n_parent, l10n_diffsource, hidden, --palette--;;time,categories',
        ]
    ],
    'palettes' => [
        'address' => ['showitem' => 'street, --linebreak--, zip,  city'],
        'data' => ['showitem' => 'latitude, longitude, address'],
        'interaction' => ['showitem' => 'open_by_click, close_by_click, opened'],
        'link' => ['showitem' => 'info_window_link'],
        'marker' => ['showitem' => 'marker, --linebreak--, image_width, image_height, --linebreak--, marker_anchor_x, marker_anchor_y, --linebreak--, popup_anchor_x, popup_anchor_y'],
        'time' => ['showitem' => 'starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_leafletosm_domain_model_address',
                'foreign_table_where' => 'AND {#tx_leafletosm_domain_model_address}.{#pid}=###CURRENT_PID### AND {#tx_leafletosm_domain_model_address}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'street' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.street',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'zip' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.zip',
            'config' => [
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim'
            ],
        ],
        'city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'configuration_map' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.configuration_map',
            'config' => [
                'type' => 'user',
                'userFunc' => Typo3graf\LeafletOsm\Utility\LocationUtility::class . '->render',
                'parameters' => [
                    'longitude' => 'longitude',
                    'latitude' => 'latitude',
                    'address' => 'address',
                    'street' => 'street',
                    'zip' => 'zip',
                    'city' => 'city',
                ],
            ],
        ],
        'latitude' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.latitude',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required,' . Typo3graf\LeafletOsm\Evaluation\Double15Evaluator::class
            ]
        ],
        'longitude' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.longitude',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required,' . Typo3graf\LeafletOsm\Evaluation\Double15Evaluator::class
            ]
        ],
        'address' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.address',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'marker' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.marker',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'marker',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'image_width' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.image_width',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'image_height' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.image_height',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'marker_anchor_x' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.marker_anchor_x',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'marker_anchor_y' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.marker_anchor_y',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'popup_anchor_x' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.popup_anchor_x',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'popup_anchor_y' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.popup_anchor_y',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'info_window_content' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.info_window_content',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'enableRichtext' => 1,
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ]
                ]
            ],
        ],
        'info_window_images' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.info_window_images',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'info_windows_images',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'close_by_click' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.close_by_click',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'open_by_click' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.open_by_click',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'opened' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_address.opened',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],

    ],
];
