<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,width,height,kml_url,max_zoom,map_types,travel_mode,measure_system',
        'iconfile' => 'EXT:leaflet_osm/Resources/Public/Icons/map.svg'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, width, height, zoom, show_addresses, show_categories, kml_url, kml_preserve_viewport, scroll_zoom, min_zoom, max_zoom, center_longitude, center_latitude, geo_location, preview_image, draggable, double_click_zoom, marker_cluster, marker_cluster_zoom, marker_cluster_size, marker_search, default_type, scale_control, fullscreen_control, zoom_control, map_type_control, map_types, show_route, calc_route, travel_mode, measure_system, map_addresses',
    ],
    'types' => [
        '0' => ['showitem' => 'title,
					--palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.palettes.size;size,
					map_addresses,
					--palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.palettes.kml;kml,
					--div--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.tab.initial,default_type,
					--palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.palettes.zoom;zoom, 
					--palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.palettes.coordinates;coordinates,
                    --palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.palettes.geolocation;geolocation,
					preview_image,
                    --div--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.tab.display,
					--palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.palettes.interaction;interaction,
					--palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.palettes.address_interaction;address_interaction,
					--palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.palettes.cluster;cluster,
					--palette--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.palettes.additional;additional,
					--div--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.tab.controls,
					--palette--;;map_control,--palette--;;controls,
					--div--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.tab.route,
					calc_route, travel_mode, measure_system, show_route,
					--div--;LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.tab.others,
					sys_language_uid, l10n_parent, l10n_diffsource, hidden, --palette--;;time'],
    ],
    'palettes' => [
        'address_interaction' => ['showitem' => 'marker_search, show_addresses, show_categories'],
        'cluster' => ['showitem' => 'marker_cluster, --linebreak--, marker_cluster_zoom, marker_cluster_size'],
        'controls' => ['showitem' => 'zoom_control, scale_control, fullscreen_control'],
        'coordinates' => ['showitem' => 'center_latitude, center_longitude'],
        'geolocation' => ['showitem' => 'geo_location'],
        'interaction' => ['showitem' => 'scroll_zoom, draggable, double_click_zoom'],
        'kml' => ['showitem' => 'kml_url, --linebreak--, kml_preserve_viewport'],
        'map_control' => ['showitem' => 'map_type_control, --linebreak--, map_types'],
        'size' => ['showitem' => 'width, height'],
        'time' => ['showitem' => 'starttime, endtime'],
        'zoom' => ['showitem' => 'zoom, --linebreak--, min_zoom, max_zoom'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_leafletosm_domain_model_map',
                'foreign_table_where' => 'AND {#tx_leafletosm_domain_model_map}.{#pid}=###CURRENT_PID### AND {#tx_leafletosm_domain_model_map}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'width' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.width',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'trim,required'
            ],
        ],
        'height' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.height',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'trim,required'
            ],
        ],
        'zoom' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.zoom',
            'config' => [
                'type' => 'input',
                'default' => 10,
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'show_addresses' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.show_addresses',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'show_categories' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.show_categories',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'kml_url' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.kml_url',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'max' => 500
            ]
        ],
        'kml_preserve_viewport' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.kml_preserve_viewport',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'scroll_zoom' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.scroll_zoom',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'min_zoom' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.min_zoom',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'max_zoom' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.max_zoom',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'trim'
            ],
        ],
        'center_longitude' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.center_longitude',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,' . Typo3graf\LeafletOsm\Evaluation\Double15Evaluator::class
            ]
        ],
        'center_latitude' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.center_latitude',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,' . Typo3graf\LeafletOsm\Evaluation\Double15Evaluator::class
            ]
        ],
        'geo_location' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.geo_location',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'preview_image' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.preview_image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'preview_image',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'draggable' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.draggable',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'double_click_zoom' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.double_click_zoom',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'marker_cluster' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.marker_cluster',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'marker_cluster_zoom' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.marker_cluster_zoom',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'marker_cluster_size' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.marker_cluster_size',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'default' => 80,
                'eval' => 'int'
            ]
        ],
        'marker_search' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.marker_search',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'default_type' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.default_type',
            'config' => [
                'type' => 'radio',
                'items' => [
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.display.default.0',
                        0
                    ],
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.display.default.1',
                        1
                    ],
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.display.default.2',
                        2
                    ],

                ],
                'eval' => '',
                'default' => 0
            ],
        ],
        'scale_control' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.scale_control',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'fullscreen_control' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.fullscreen_control',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'zoom_control' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.zoom_control',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'map_type_control' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.map_type_control',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'map_types' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.map_types',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'items' => [
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.map_types.0',
                        0
                    ],
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.map_types.1',
                        1
                    ],
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.map_types.2',
                        2
                    ],
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.map_types.3',
                        3
                    ],
                ],
                'size' => 5,
                'default' => '0,1,2',
                'maxitems' => 5,
                'eval' => '',
            ],
        ],
        'show_route' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.show_route',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'calc_route' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.calc_route',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'travel_mode' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.travel_mode',
            'config' => [
                'type' => 'radio',
                'items' => [
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.travel_mode.0',
                        0
                    ],
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.travel_mode.1',
                        1
                    ],
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.travel_mode.2',
                        2
                    ],
                ],
                'eval' => '',
                'default' => 0
            ],
        ],
        'measure_system' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.measure_system',
            'config' => [
                'type' => 'radio',
                'items' => [
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.unit_system.0',
                        0
                    ],
                    [
                        'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.unit_system.1',
                        1
                    ],

                ],
                'eval' => '',
                'default' => 2
            ],
        ],
        'map_addresses' => [
            'exclude' => true,
            'label' => 'LLL:EXT:leaflet_osm/Resources/Private/Language/locallang_db.xlf:tx_leafletosm_domain_model_map.map_addresses',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_leafletosm_domain_model_address',
                'foreign_table' => 'tx_leafletosm_domain_model_address',
                'MM' => 'tx_leafletosm_map_address_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                ],
            ],

        ],

    ],
];
