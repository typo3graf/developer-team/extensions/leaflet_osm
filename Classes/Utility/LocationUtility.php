<?php

namespace Typo3graf\LeafletOsm\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Mike Tölle <mtoelle@typo3graf.de>, Typo3graf media-agentur UG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class LocationUtility
{
    /**
     * Renders the OpenStreet map in BE.
     *
     * @param array $PA
     * @param \TYPO3\CMS\Backend\Form\Element\UserElement $pObj
     * @return string
     * @throws \Exception
     */
    public function render(array &$PA, $pObj)
    {
        $pageRenderer  =  GeneralUtility :: makeInstance ( PageRenderer :: class );
        $pageRenderer -> loadRequireJsModule ( '/typo3conf/ext/leaflet_osm/Resources/Public/Contrib/leaflet/leaflet.js' );
        //$pathPrefix =  PathUtility::getAbsoluteWebPath(ExtensionManagementUtility::extPath($this->request->getControllerExtensionKey()));
        //  \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($pObj);
        // \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($PA); die();
        $latitude = (float)$PA['row'][$PA['parameters']['latitude']];
        $longitude = (float)$PA['row'][$PA['parameters']['longitude']];
        $address = $PA['row'][$PA['parameters']['address']];

        $baseElementId = isset($PA['itemFormElID']) ? $PA['itemFormElID'] : $PA['table'] . '_map';
        $addressId = $baseElementId . '_address';
        $mapId = $baseElementId . '_map';

        if (!($latitude && $longitude)) {
            $latitude = 0;
            $longitude = 0;
        }
        $dataPrefix = 'data[' . $PA['table'] . '][' . $PA['row']['uid'] . ']';
        $latitudeField = $dataPrefix . '[' . $PA['parameters']['latitude'] . ']';
        $longitudeField = $dataPrefix . '[' . $PA['parameters']['longitude'] . ']';
        $addressField = $dataPrefix . '[' . $PA['parameters']['address'] . ']';
        $streetFieldName = $dataPrefix . '[' . $PA['parameters']['street'] . ']';
        $zipFieldName = $dataPrefix . '[' . $PA['parameters']['zip'] . ']';
        $cityFieldName = $dataPrefix . '[' . $PA['parameters']['city'] . ']';

        $updateJs = "TBE_EDITOR.fieldChanged('%s','%s','%s','%s');";
        $updateLatitudeJs = sprintf(
            $updateJs,
            $PA['table'],
            $PA['row']['uid'],
            $PA['parameters']['latitude'],
            $latitudeField
        );
        $updateLongitudeJs = sprintf(
            $updateJs,
            $PA['table'],
            $PA['row']['uid'],
            $PA['parameters']['longitude'],
            $longitudeField
        );
        $updateAddressJs = sprintf(
            $updateJs,
            $PA['table'],
            $PA['row']['uid'],
            $PA['parameters']['address'],
            $addressField
        );
        $out = [];


        $out [] = '<script type="text/javascript">';
        $out [] = <<<EOT
                if (typeof TxLeafletAddress == 'undefined') TxLeafletAddress = {};

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); } 

TxLeafletAddress.init = function() {
var osmIcon = L.icon({
    iconUrl: '/typo3conf/ext/leaflet_osm/Resources/Public/Contrib/leaflet/images/osm-marker-icon.png',
});
	TxLeafletAddress.map = L.map('{$mapId}',{
	zoomControl: 1,
	scrollWheelZoom: 1,
	dragging: 1,
	doubleClickZoom: 1,
	}).setView([{$latitude},{$longitude}], 13);
	L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(TxLeafletAddress.map);

if(document.getElementsByName("{$streetFieldName}")[0] && 
	    document.getElementsByName("{$streetFieldName}")[0] && 
	    document.getElementsByName("{$streetFieldName}")[0]) {
	    var button = document.getElementById('osm-btn-address').style.display = 'inline-block';
	}
	
TxLeafletAddress.marker = L.marker([{$latitude},{$longitude}],{draggable:true, autoPan:true}).addTo(TxLeafletAddress.map);

TxLeafletAddress.marker.on('dragend', function(event){
    TxLeafletAddress.marker = event.target;
    //TxLeafletAddress.marker.setIcon(L.Icon.Default);
    var position = TxLeafletAddress.marker.getLatLng();
    TxLeafletAddress.marker.setLatLng(new L.LatLng(position.lat, position.lng),{draggable:'true'});
    // update fields
    TxLeafletAddress.updateValue('{$latitudeField}', position.lat);
    TxLeafletAddress.updateValue('{$longitudeField}', position.lng);
    // Update address
    TxLeafletAddress.reverseGeocode(position.lat,position.lng);
    // Update Position
		var newPosition = document.getElementById("{$addressId}");
		newPosition.value = position.lat + "," + position.lng;
    // Tell TYPO3 that fields were updated
		TxLeafletAddress.positionChanged();
  }); //marker.on dragend
	}; // init
	
	TxLeafletAddress.localize = function(address) {
	$.getJSON("https://nominatim.openstreetmap.org/search?q="+address+"&format=json&addressdetails=1", function(data,status) {
	
	  if (status == 'success'){
	    
	  // update map
	  TxLeafletAddress.map.setView([data[0].lat,data[0].lon], 18);
	  TxLeafletAddress.marker.setLatLng([data[0].lat,data[0].lon]);
	  
	  // update fields
	  TxLeafletAddress.updateValue('{$latitudeField}', data[0].lat);
      TxLeafletAddress.updateValue('{$longitudeField}', data[0].lon);
	  TxLeafletAddress.updateValue('{$addressField}', data[0].display_name);
	  TxLeafletAddress.positionChanged();
	  } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
	});
	}
	
	TxLeafletAddress.codeAddress = function() {
	var address = document.getElementById("{$addressId}").value;
	var lat = 0;
	var lng = 0;
	if(address.match(/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/)) {
		// Get Position
		lat = address.substr(0, address.lastIndexOf(',')).trim();
		lng = address.substr(address.lastIndexOf(',')+1).trim();
				 // update map
	  TxLeafletAddress.map.setView([lat,lng], 18);
	  TxLeafletAddress.marker.setLatLng([lat,lng]);

		// Update visible fields
		TxLeafletAddress.updateValue('{$latitudeField}', lat);
        TxLeafletAddress.updateValue('{$longitudeField}', lng);

		// Get Address
		TxLeafletAddress.reverseGeocode(lat, lng);
	} else {
		TxLeafletAddress.localize(address);
	}
	}
	
	TxLeafletAddress.codeByAddress = function() {
	var street = document.getElementsByName("{$streetFieldName}")[0].value,
	    zip = document.getElementsByName("{$zipFieldName}")[0].value,
	    city = document.getElementsByName("{$cityFieldName}")[0].value,
	    address = encodeURIComponent(street.trim()) + ',' + zip + '%20' + city;
	    
	    TxLeafletAddress.localize(address);
	}
	
	TxLeafletAddress.positionChanged = function() {
    {$updateLatitudeJs}
    {$updateLongitudeJs}
    {$updateAddressJs}
    TYPO3.FormEngine.Validation.validate();
}

	TxLeafletAddress.updateValue = function(fieldName, value) {

	document[TBE_EDITOR.formname][fieldName].value = value;
    window.$('[data-formengine-input-name="' + fieldName + '"]').val(value);
	}
		
	TxLeafletAddress.reverseGeocode = function(lat,lng) {
	$.getJSON("https://nominatim.openstreetmap.org/reverse?format=json&lat="+lat+"&lon="+lng+"&zoom=18&addressdetails=1", function(data,status) {
	  if (status == 'success' && typeof data.lat != 'undefined'){
	  TxLeafletAddress.updateValue('{$addressField}', data.display_name);
	  TxLeafletAddress.positionChanged();
	  }
    });
	}
	
	/*TxLeafletAddress.convertAddress = function(address) {
	splitAddress = address.split(',');
	console.log(splitAddress);
	return address;
	}*/
	
	window.onload = TxLeafletAddress.init;

EOT;
        $out[] = '</script>';
        $out[] = '<div id="' . $baseElementId . '">';
        $out[] = '
			<input type="text" 
			       class="form-control" 
			       value="' . $address . '" 
			       id="' . $addressId . '" 
			       style="display:inline-block;width:300px">
			<input type="button" 
			       value="' . LocalizationUtility::translate('update_by_position', 'leaflet_osm') . '" 
			       class="btn btn-sm btn-default"
			       onclick="TxLeafletAddress.codeAddress()">
			<input id="osm-btn-address"
			       type="button" 
			       value="' . LocalizationUtility::translate('update_by_address', 'leaflet_osm') . '" 
			       class="btn btn-sm btn-default"
			       style="display:none;"
			       onclick="TxLeafletAddress.codeByAddress()">
		';
        $out[] = '<div id="' . $mapId . '" style="height:400px;margin:10px 0;width:400px"></div>';
        $out[] = '</div>'; // id=$baseElementId
        return implode('', $out);
    }
}
