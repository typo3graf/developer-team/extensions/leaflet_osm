<?php
namespace Typo3graf\LeafletOsm\Controller;


/***
 *
 * This file is part of the "Leaflet-OpenStreetMaps" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Typo3graf Developer-Team <development@typo3graf.de>, Typo3graf media-agentur
 *
 ***/

use Typo3graf\LeafletOsm\Domain\Model\Map;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * MapController
 */
class MapController extends ActionController
{

    /**
     * mapRepository
     *
     * @var \Typo3graf\LeafletOsm\Domain\Repository\MapRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $mapRepository = null;

    /**
     * addressRepository
     *
     * @var \Typo3graf\LeafletOsm\Domain\Repository\AddressRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $addressRepository;

    /**
     * @var string
     */
    protected $leafletLibrary;

    public function initializeAction()
    {
       // $extConf = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('leaflet_osm');

        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);


       /* if ($extConf['footerJS'] === '0') {
            $addJsMethod = 'addJs';
        }*/

        $this->leafletLibrary = 'EXT:leaflet_osm/Resources/Public/Contrib/leaflet/leaflet.js';

       /* if(!$this->settings['preview']['enabled']) {
            $pageRenderer->{$addJsMethod . 'Library'}(
                'googleMaps',
                $this->googleMapsLibrary,
                'text/javascript',
                false,
                false,
                '',
                true
            );
        }
*/
        $pathPrefix =  PathUtility::getAbsoluteWebPath(ExtensionManagementUtility::extPath($this->request->getControllerExtensionKey()));
        /*if ($extConf['include_library'] === '1') {
            $pageRenderer->{$addJsMethod . 'Library'}(
                'jQuery',
                $pathPrefix . 'Resources/Public/Scripts/jquery.min.js'
            );
        }*/

             $pageRenderer->addJsFooterLibrary('leaflet',$pathPrefix . 'Resources/Public/Contrib/leaflet/leaflet.js');
             $pageRenderer->addJsFooterLibrary('leaflet_providers',$pathPrefix . 'Resources/Public/Contrib/leaflet/leaflet-providers.js');
             $pageRenderer->addJsFooterLibrary( 'leaflet_fullscreen',$pathPrefix . 'Resources/Public/Contrib/leaflet_fullscreen/leaflet_fullscreen.js');
             $pageRenderer->addJsFooterLibrary( 'leaflet_Locate',$pathPrefix . 'Resources/Public/Contrib/leaflet_geolocation/L.Control.Locate.js');
             $pageRenderer->addJsFooterLibrary( 'leaflet_MarkerCluster',$pathPrefix . 'Resources/Public/Contrib/leaflet_markerCluster/MarkerCluster.js');
             $pageRenderer->addJsFooterLibrary( 'leaflet_Search',$pathPrefix . 'Resources/Public/Contrib/leaflet_search/leaflet-search.js');
             $pageRenderer->addJsFooterLibrary( 'leaflet_KML',$pathPrefix . 'Resources/Public/Contrib/leaflet_kml/KML.js');
        $pageRenderer->addJsFooterLibrary( 'leaflet_lodash',$pathPrefix . 'Resources/Public/Contrib/leaflet_routing/lodash.js');
        $pageRenderer->addJsFooterLibrary( 'leaflet_corslite',$pathPrefix . 'Resources/Public/Contrib/leaflet_routing/corslite.js');
        $pageRenderer->addJsFooterLibrary( 'leaflet_polyline',$pathPrefix . 'Resources/Public/Contrib/leaflet_routing/polyline.js');

        $pageRenderer->addJsFooterLibrary( 'leaflet_geocoder',$pathPrefix . 'Resources/Public/Contrib/leaflet_routing/Geocoder.js');
             $pageRenderer->addJsFooterLibrary( 'leaflet_routing',$pathPrefix . 'Resources/Public/Contrib/leaflet_routing/leaflet-routing-machine.js');
        $pageRenderer->addJsFooterLibrary( 'leaflet_openrouteservice',$pathPrefix . 'Resources/Public/Contrib/leaflet_routing/OpenRouteService.js');
        $pageRenderer->addJsFooterLibrary( 'leaflet_categories',$pathPrefix . 'Resources/Public/Contrib/leaflet_categories/leaflet_subgroup.js');

       if($this->settings['preview']['setCookieToShowMapAlways']) {
           $pageRenderer->addJsFooterLibrary('leaflet_cookie', $pathPrefix . 'Resources/Public/JavaScript/jquery.cookie.js');;
          }
          if($this->settings['preview']['enabled']) {
              $pageRenderer->addJsFooterLibrary('leaflet_preview', $pathPrefix . 'Resources/Public/JavaScript/jquery.leafletosm.preview.js');
          }
        $pageRenderer->addCssLibrary( $pathPrefix. 'Resources/Public/Fonts/all.css');
        $pageRenderer->addCssLibrary($pathPrefix. 'Resources/Public/Contrib/leaflet/leaflet.css');
        $pageRenderer->addCssLibrary($pathPrefix. 'Resources/Public/Contrib/leaflet_fullscreen/leaflet_fullscreen.css');
        $pageRenderer->addCssLibrary($pathPrefix. 'Resources/Public/Contrib/leaflet_geolocation/Locate.css');
        $pageRenderer->addCssLibrary($pathPrefix. 'Resources/Public/Contrib/leaflet_markerCluster/MarkerCluster.css');
        $pageRenderer->addCssLibrary($pathPrefix. 'Resources/Public/Contrib/leaflet_markerCluster/MarkerCluster.Default.css');
        $pageRenderer->addCssLibrary($pathPrefix. 'Resources/Public/Contrib/leaflet_search/leaflet-search.css');
        $pageRenderer->addCssLibrary($pathPrefix. 'Resources/Public/Contrib/leaflet_routing/Geocoder.css');
        $pageRenderer->addCssLibrary($pathPrefix. 'Resources/Public/Contrib/leaflet_routing/leaflet-routing-machine.css');
        }/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */

    /**
     * action show
     *
     * @param \Typo3graf\LeafletOsm\Domain\Model\Map $map
     * @return void
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function showAction(Map $map = null)
    {
        $categoriesArray = [];
        // Get the data object (contains the tt_content fields)
        $data = $this->configurationManager->getContentObject()->data;

        // Append flexform values
        $this->configurationManager->getContentObject()->readFlexformIntoConf($data['pi_flexform'], $mapId);

        $map = $map ?? $this->mapRepository->findByUid($mapId['settings.map']);

        // find addresses
        $addresses = $map->getMapAddresses();
        // no addresses related to the map, try to find some from the storagePid
        if ($addresses->count() === 0 && $this->settings['storagePid']) {
            $pid = str_ireplace('this', $GLOBALS['TSFE']->id, $this->settings['storagePid']);
            $addresses = $this->addressRepository->findAllAddresses($map, $pid);
        }

        // get categories
        if ($map->isShowCategories()) {
            foreach ($addresses as $address) {
                /* @var \Typo3graf\LeafletOsm\Domain\Model\Address $address */

                $addressCategories = $address->getCategories();
                /* @var \Typo3graf\LeafletOsm\Domain\Model\Category $addressCategory */
                foreach ($addressCategories as $addressCategory) {
                    $categoriesArray[$addressCategory->getUid()] = $addressCategory;
                }
            }
        }
        // set map map marker

        $bounds_code = '';
        foreach ($addresses as $address) {
            //  \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($address);
            $bounds_code .= '[' . $address->getLatitude() . ',' . $address->getLongitude() . '],';
        }

        $this->view->assignMultiple([
            'map' => $map,
            'boundsCode' => $bounds_code,
            'mapTypes' => explode ( ',' , $map->getMapTypes()  ),
            'addresses' => $addresses,
            'categories' => $categoriesArray,
        ]);
    }
}
