<?php
namespace Typo3graf\LeafletOsm\Domain\Repository;


/***
 *
 * This file is part of the "Leaflet-OpenStreetMaps" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Typo3graf Developer-Team <development@typo3graf.de>, Typo3graf media-agentur
 *
 ***/
/**
 * The repository for Maps
 */
class MapRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = [
    'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
];
}
