<?php
namespace Typo3graf\LeafletOsm\Domain\Repository;


/***
 *
 * This file is part of the "Leaflet-OpenStreetMaps" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Typo3graf Developer-Team <development@typo3graf.de>, Typo3graf media-agentur
 *
 ***/

use Typo3graf\LeafletOsm\Domain\Model\Map;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * The repository for Addresses
 */
class AddressRepository extends Repository
{

    /**
     * Finds all addresses by the specified map or the storage pid
     *
     * @param \Typo3graf\LeafletOsm\Domain\Model\Map $map The map
     * @param int $pid The Storage Pid
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The addresses
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findAllAddresses(Map $map, $pid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);

        $or = [];
        $and = [];

        foreach (explode(',', $pid) as $p) {
            $or[] = $query->equals('pid', $p);
        }
        if ($map) {
            $or[] = $query->contains('map', $map);
        }
        $and[] = $query->logicalOr($or);

        return $query->matching(
            $query->logicalAnd(
                $and
            )
        )
            ->execute();
    }
}
