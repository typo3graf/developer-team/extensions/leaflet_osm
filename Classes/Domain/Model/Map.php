<?php
namespace Typo3graf\LeafletOsm\Domain\Model;


/***
 *
 * This file is part of the "Leaflet-OpenStreetMaps" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Typo3graf Developer-Team <development@typo3graf.de>, Typo3graf media-agentur
 *
 ***/
/**
 * Map
 */
class Map extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * width
     *
     * @var string
     * @validate NotEmpty
     */
    protected $width = '';

    /**
     * height
     *
     * @var string
     * @validate NotEmpty
     */
    protected $height = '';

    /**
     * zoom
     *
     * @var int
     */
    protected $zoom = 10;

    /**
     * showAddresses
     *
     * @var bool
     */
    protected $showAddresses = false;

    /**
     * showCategories
     *
     * @var bool
     */
    protected $showCategories = false;

    /**
     * kmlUrl
     *
     * @var string
     */
    protected $kmlUrl = '';

    /**
     * kmlPreserveViewport
     *
     * @var bool
     */
    protected $kmlPreserveViewport = false;

    /**
     * scrollZoom
     *
     * @var bool
     */
    protected $scrollZoom = false;

    /**
     * minZoom
     *
     * @var int
     */
    protected $minZoom = 0;

    /**
     * maxZoom
     *
     * @var int
     */
    protected $maxZoom = 0;

    /**
     * centerLongitude
     *
     * @var float
     */
    protected $centerLongitude = 0.0;

    /**
     * centerLatitude
     *
     * @var float
     */
    protected $centerLatitude = 0.0;

    /**
     * geoLocation
     *
     * @var bool
     */
    protected $geoLocation = false;

    /**
     * previewImage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $previewImage = null;

    /**
     * draggable
     *
     * @var bool
     */
    protected $draggable = false;

    /**
     * doubleClickZoom
     *
     * @var bool
     */
    protected $doubleClickZoom = false;

    /**
     * markerCluster
     *
     * @var bool
     */
    protected $markerCluster = false;

    /**
     * markerClusterZoom
     *
     * @var int
     */
    protected $markerClusterZoom = 0;

    /**
     * markerClusterSize
     *
     * @var int
     */
    protected $markerClusterSize = 0;

    /**
     * markerSearch
     *
     * @var bool
     */
    protected $markerSearch = false;

    /**
     * defaultType
     *
     * @var int
     */
    protected $defaultType = 0;

    /**
     * scaleControl
     *
     * @var bool
     */
    protected $scaleControl = false;

    /**
     * fullscreenControl
     *
     * @var bool
     */
    protected $fullscreenControl = false;

    /**
     * zoomControl
     *
     * @var bool
     */
    protected $zoomControl = false;

    /**
     * mapTypeControl
     *
     * @var bool
     */
    protected $mapTypeControl = false;

    /**
     * mapTypes
     *
     * @var string
     */
    protected $mapTypes = '';

    /**
     * showRoute
     *
     * @var bool
     */
    protected $showRoute = false;

    /**
     * calcRoute
     *
     * @var bool
     */
    protected $calcRoute = false;

    /**
     * travelMode
     *
     * @var string
     */
    protected $travelMode = '';

    /**
     * measureSystem
     *
     * @var string
     */
    protected $measureSystem = '';

    /**
     * mapAddresses
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Typo3graf\LeafletOsm\Domain\Model\Address>
     */
    protected $mapAddresses = null;

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->mapAddresses = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the width
     *
     * @return string $width
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Sets the width
     *
     * @param string $width
     * @return void
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * Returns the height
     *
     * @return string $height
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Sets the height
     *
     * @param string $height
     * @return void
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * Returns the zoom
     *
     * @return int $zoom
     */
    public function getZoom()
    {
        return $this->zoom;
    }

    /**
     * Sets the zoom
     *
     * @param int $zoom
     * @return void
     */
    public function setZoom($zoom)
    {
        $this->zoom = $zoom;
    }

    /**
     * Returns the showAddresses
     *
     * @return bool $showAddresses
     */
    public function getShowAddresses()
    {
        return $this->showAddresses;
    }

    /**
     * Sets the showAddresses
     *
     * @param bool $showAddresses
     * @return void
     */
    public function setShowAddresses($showAddresses)
    {
        $this->showAddresses = $showAddresses;
    }

    /**
     * Returns the boolean state of showAddresses
     *
     * @return bool
     */
    public function isShowAddresses()
    {
        return $this->showAddresses;
    }

    /**
     * Returns the showCategories
     *
     * @return bool $showCategories
     */
    public function getShowCategories()
    {
        return $this->showCategories;
    }

    /**
     * Sets the showCategories
     *
     * @param bool $showCategories
     * @return void
     */
    public function setShowCategories($showCategories)
    {
        $this->showCategories = $showCategories;
    }

    /**
     * Returns the boolean state of showCategories
     *
     * @return bool
     */
    public function isShowCategories()
    {
        return $this->showCategories;
    }

    /**
     * Returns the kmlUrl
     *
     * @return string $kmlUrl
     */
    public function getKmlUrl()
    {
        return $this->kmlUrl;
    }

    /**
     * Sets the kmlUrl
     *
     * @param string $kmlUrl
     * @return void
     */
    public function setKmlUrl($kmlUrl)
    {
        $this->kmlUrl = $kmlUrl;
    }

    /**
     * Returns the kmlPreserveViewport
     *
     * @return bool $kmlPreserveViewport
     */
    public function getKmlPreserveViewport()
    {
        return $this->kmlPreserveViewport;
    }

    /**
     * Sets the kmlPreserveViewport
     *
     * @param bool $kmlPreserveViewport
     * @return void
     */
    public function setKmlPreserveViewport($kmlPreserveViewport)
    {
        $this->kmlPreserveViewport = $kmlPreserveViewport;
    }

    /**
     * Returns the boolean state of kmlPreserveViewport
     *
     * @return bool
     */
    public function isKmlPreserveViewport()
    {
        return $this->kmlPreserveViewport;
    }

    /**
     * Returns the scrollZoom
     *
     * @return bool $scrollZoom
     */
    public function getScrollZoom()
    {
        return $this->scrollZoom;
    }

    /**
     * Sets the scrollZoom
     *
     * @param bool $scrollZoom
     * @return void
     */
    public function setScrollZoom($scrollZoom)
    {
        $this->scrollZoom = $scrollZoom;
    }

    /**
     * Returns the boolean state of scrollZoom
     *
     * @return bool
     */
    public function isScrollZoom()
    {
        return $this->scrollZoom;
    }

    /**
     * Returns the minZoom
     *
     * @return int $minZoom
     */
    public function getMinZoom()
    {
        return $this->minZoom;
    }

    /**
     * Sets the minZoom
     *
     * @param int $minZoom
     * @return void
     */
    public function setMinZoom($minZoom)
    {
        $this->minZoom = $minZoom;
    }

    /**
     * Returns the maxZoom
     *
     * @return int $maxZoom
     */
    public function getMaxZoom()
    {
        return $this->maxZoom;
    }

    /**
     * Sets the maxZoom
     *
     * @param int $maxZoom
     * @return void
     */
    public function setMaxZoom($maxZoom)
    {
        $this->maxZoom = $maxZoom;
    }

    /**
     * Returns the centerLongitude
     *
     * @return float $centerLongitude
     */
    public function getCenterLongitude()
    {
        return $this->centerLongitude;
    }

    /**
     * Sets the centerLongitude
     *
     * @param float $centerLongitude
     * @return void
     */
    public function setCenterLongitude($centerLongitude)
    {
        $this->centerLongitude = $centerLongitude;
    }

    /**
     * Returns the centerLatitude
     *
     * @return float $centerLatitude
     */
    public function getCenterLatitude()
    {
        return $this->centerLatitude;
    }

    /**
     * Sets the centerLatitude
     *
     * @param float $centerLatitude
     * @return void
     */
    public function setCenterLatitude($centerLatitude)
    {
        $this->centerLatitude = $centerLatitude;
    }

    /**
     * Returns the geoLocation
     *
     * @return bool $geoLocation
     */
    public function getGeoLocation()
    {
        return $this->geoLocation;
    }

    /**
     * Sets the geoLocation
     *
     * @param bool $geoLocation
     * @return void
     */
    public function setGeoLocation($geoLocation)
    {
        $this->geoLocation = $geoLocation;
    }

    /**
     * Returns the boolean state of geoLocation
     *
     * @return bool
     */
    public function isGeoLocation()
    {
        return $this->geoLocation;
    }

    /**
     * Returns the previewImage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $previewImage
     */
    public function getPreviewImage()
    {
        return $this->previewImage;
    }

    /**
     * Sets the previewImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $previewImage
     * @return void
     */
    public function setPreviewImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $previewImage)
    {
        $this->previewImage = $previewImage;
    }

    /**
     * Returns the draggable
     *
     * @return bool $draggable
     */
    public function getDraggable()
    {
        return $this->draggable;
    }

    /**
     * Sets the draggable
     *
     * @param bool $draggable
     * @return void
     */
    public function setDraggable($draggable)
    {
        $this->draggable = $draggable;
    }

    /**
     * Returns the boolean state of draggable
     *
     * @return bool
     */
    public function isDraggable()
    {
        return $this->draggable;
    }

    /**
     * Returns the doubleClickZoom
     *
     * @return bool $doubleClickZoom
     */
    public function getDoubleClickZoom()
    {
        return $this->doubleClickZoom;
    }

    /**
     * Sets the doubleClickZoom
     *
     * @param bool $doubleClickZoom
     * @return void
     */
    public function setDoubleClickZoom($doubleClickZoom)
    {
        $this->doubleClickZoom = $doubleClickZoom;
    }

    /**
     * Returns the boolean state of doubleClickZoom
     *
     * @return bool
     */
    public function isDoubleClickZoom()
    {
        return $this->doubleClickZoom;
    }

    /**
     * Returns the markerCluster
     *
     * @return bool $markerCluster
     */
    public function getMarkerCluster()
    {
        return $this->markerCluster;
    }

    /**
     * Sets the markerCluster
     *
     * @param bool $markerCluster
     * @return void
     */
    public function setMarkerCluster($markerCluster)
    {
        $this->markerCluster = $markerCluster;
    }

    /**
     * Returns the boolean state of markerCluster
     *
     * @return bool
     */
    public function isMarkerCluster()
    {
        return $this->markerCluster;
    }

    /**
     * Returns the markerClusterZoom
     *
     * @return int $markerClusterZoom
     */
    public function getMarkerClusterZoom()
    {
        return $this->markerClusterZoom;
    }

    /**
     * Sets the markerClusterZoom
     *
     * @param int $markerClusterZoom
     * @return void
     */
    public function setMarkerClusterZoom($markerClusterZoom)
    {
        $this->markerClusterZoom = $markerClusterZoom;
    }

    /**
     * Returns the markerClusterSize
     *
     * @return int $markerClusterSize
     */
    public function getMarkerClusterSize()
    {
        return $this->markerClusterSize;
    }

    /**
     * Sets the markerClusterSize
     *
     * @param int $markerClusterSize
     * @return void
     */
    public function setMarkerClusterSize($markerClusterSize)
    {
        $this->markerClusterSize = $markerClusterSize;
    }

    /**
     * Returns the markerSearch
     *
     * @return bool $markerSearch
     */
    public function getMarkerSearch()
    {
        return $this->markerSearch;
    }

    /**
     * Sets the markerSearch
     *
     * @param bool $markerSearch
     * @return void
     */
    public function setMarkerSearch($markerSearch)
    {
        $this->markerSearch = $markerSearch;
    }

    /**
     * Returns the boolean state of markerSearch
     *
     * @return bool
     */
    public function isMarkerSearch()
    {
        return $this->markerSearch;
    }

    /**
     * Returns the defaultType
     *
     * @return int $defaultType
     */
    public function getDefaultType()
    {
        return $this->defaultType;
    }

    /**
     * Sets the defaultType
     *
     * @param int $defaultType
     * @return void
     */
    public function setDefaultType($defaultType)
    {
        $this->defaultType = $defaultType;
    }

    /**
     * Returns the scaleControl
     *
     * @return bool $scaleControl
     */
    public function getScaleControl()
    {
        return $this->scaleControl;
    }

    /**
     * Sets the scaleControl
     *
     * @param bool $scaleControl
     * @return void
     */
    public function setScaleControl($scaleControl)
    {
        $this->scaleControl = $scaleControl;
    }

    /**
     * Returns the boolean state of scaleControl
     *
     * @return bool
     */
    public function isScaleControl()
    {
        return $this->scaleControl;
    }

    /**
     * Returns the fullscreenControl
     *
     * @return bool $fullscreenControl
     */
    public function getFullscreenControl()
    {
        return $this->fullscreenControl;
    }

    /**
     * Sets the fullscreenControl
     *
     * @param bool $fullscreenControl
     * @return void
     */
    public function setFullscreenControl($fullscreenControl)
    {
        $this->fullscreenControl = $fullscreenControl;
    }

    /**
     * Returns the boolean state of fullscreenControl
     *
     * @return bool
     */
    public function isFullscreenControl()
    {
        return $this->fullscreenControl;
    }

    /**
     * Returns the zoomControl
     *
     * @return bool $zoomControl
     */
    public function getZoomControl()
    {
        return $this->zoomControl;
    }

    /**
     * Sets the zoomControl
     *
     * @param bool $zoomControl
     * @return void
     */
    public function setZoomControl($zoomControl)
    {
        $this->zoomControl = $zoomControl;
    }

    /**
     * Returns the boolean state of zoomControl
     *
     * @return bool
     */
    public function isZoomControl()
    {
        return $this->zoomControl;
    }

    /**
     * Returns the mapTypeControl
     *
     * @return bool $mapTypeControl
     */
    public function getMapTypeControl()
    {
        return $this->mapTypeControl;
    }

    /**
     * Sets the mapTypeControl
     *
     * @param bool $mapTypeControl
     * @return void
     */
    public function setMapTypeControl($mapTypeControl)
    {
        $this->mapTypeControl = $mapTypeControl;
    }

    /**
     * Returns the boolean state of mapTypeControl
     *
     * @return bool
     */
    public function isMapTypeControl()
    {
        return $this->mapTypeControl;
    }

    /**
     * Returns the mapTypes
     *
     * @return string $mapTypes
     */
    public function getMapTypes()
    {
        return $this->mapTypes;
    }

    /**
     * Sets the mapTypes
     *
     * @param string $mapTypes
     * @return void
     */
    public function setMapTypes($mapTypes)
    {
        $this->mapTypes = $mapTypes;
    }

    /**
     * Returns the showRoute
     *
     * @return bool $showRoute
     */
    public function getShowRoute()
    {
        return $this->showRoute;
    }

    /**
     * Sets the showRoute
     *
     * @param bool $showRoute
     * @return void
     */
    public function setShowRoute($showRoute)
    {
        $this->showRoute = $showRoute;
    }

    /**
     * Returns the boolean state of showRoute
     *
     * @return bool
     */
    public function isShowRoute()
    {
        return $this->showRoute;
    }

    /**
     * Returns the calcRoute
     *
     * @return bool $calcRoute
     */
    public function getCalcRoute()
    {
        return $this->calcRoute;
    }

    /**
     * Sets the calcRoute
     *
     * @param bool $calcRoute
     * @return void
     */
    public function setCalcRoute($calcRoute)
    {
        $this->calcRoute = $calcRoute;
    }

    /**
     * Returns the boolean state of calcRoute
     *
     * @return bool
     */
    public function isCalcRoute()
    {
        return $this->calcRoute;
    }

    /**
     * Returns the travelMode
     *
     * @return string $travelMode
     */
    public function getTravelMode()
    {
        return $this->travelMode;
    }

    /**
     * Sets the travelMode
     *
     * @param string $travelMode
     * @return void
     */
    public function setTravelMode($travelMode)
    {
        $this->travelMode = $travelMode;
    }

    /**
     * Adds a Address
     *
     * @param \Typo3graf\LeafletOsm\Domain\Model\Address $mapAddress
     * @return void
     */
    public function addMapAddress(\Typo3graf\LeafletOsm\Domain\Model\Address $mapAddress)
    {
        $this->mapAddresses->attach($mapAddress);
    }

    /**
     * Removes a Address
     *
     * @param \Typo3graf\LeafletOsm\Domain\Model\Address $mapAddressToRemove The Address to be removed
     * @return void
     */
    public function removeMapAddress(\Typo3graf\LeafletOsm\Domain\Model\Address $mapAddressToRemove)
    {
        $this->mapAddresses->detach($mapAddressToRemove);
    }

    /**
     * Returns the mapAddresses
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Typo3graf\LeafletOsm\Domain\Model\Address> $mapAddresses
     */
    public function getMapAddresses()
    {
        return $this->mapAddresses;
    }

    /**
     * Sets the mapAddresses
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Typo3graf\LeafletOsm\Domain\Model\Address> $mapAddresses
     * @return void
     */
    public function setMapAddresses(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $mapAddresses)
    {
        $this->mapAddresses = $mapAddresses;
    }

    /**
     * Returns the measureSystem
     *
     * @return string measureSystem
     */
    public function getMeasureSystem()
    {
        return $this->measureSystem;
    }

    /**
     * Sets the measureSystem
     *
     * @param string $measureSystem
     * @return void
     */
    public function setMeasureSystem($measureSystem)
    {
        $this->measureSystem = $measureSystem;
    }
}
