<?php /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */

namespace Typo3graf\LeafletOsm\Domain\Model;


/***
 *
 * This file is part of the "Leaflet-OpenStreetMaps" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Typo3graf Developer-Team <development@typo3graf.de>, Typo3graf media-agentur
 *
 ***/

use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Address
 */
class Address extends AbstractEntity
{

    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * street
     *
     * @var string
     */
    protected $street = '';

    /**
     * zip
     *
     * @var string
     */
    protected $zip = '';

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * configurationMap
     *
     * @var string
     */
    protected $configurationMap = '';

    /**
     * latitude
     *
     * @var float
     */
    protected $latitude = 0.0;

    /**
     * longitude
     *
     * @var float
     */
    protected $longitude = 0.0;

    /**
     * address
     *
     * @var string
     * @validate NotEmpty
     */
    protected $address = '';

    /**
     * marker
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $marker = null;

    /**
     * imageSize
     *
     * @var bool
     */
    protected $imageSize = false;

    /**
     * imageWidth
     *
     * @var int
     */
    protected $imageWidth = 0;

    /**
     * imageHeight
     *
     * @var int
     */
    protected $imageHeight = 0;

    /**
     * markerAnchorX
     *
     * @var int
     */
    protected $markerAnchorX = 0;

    /**
     * markerAnchorY
     *
     * @var int
     */
    protected $markerAnchorY = 0;

    /**
     * popupAnchorX
     *
     * @var int
     */
    protected $popupAnchorX = 0;

    /**
     * popupAnchorY
     *
     * @var int
     */
    protected $popupAnchorY = 0;

    /**
     * infoWindowContent
     *
     * @var string
     */
    protected $infoWindowContent = '';

    /**
     * infoWindowImages
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $infoWindowImages = null;

    /**
     * closeByClick
     *
     * @var bool
     */
    protected $closeByClick = false;

    /**
     * openByClick
     *
     * @var bool
     */
    protected $openByClick = false;

    /**
     * opened
     *
     * @var bool
     */
    protected $opened = false;
    /**
     * categories
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Typo3graf\LeafletOsm\Domain\Model\Category>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $categories;


    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all \TYPO3\CMS\Extbase\Persistence\ObjectStorage properties.
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        /**
         * Do not modify this method!
         * It will be rewritten on each save in the extension builder
         * You may modify the constructor of this class instead
         */
        $this->infoWindowImages = new ObjectStorage();
        $this->categories = new ObjectStorage();
        //$this->map = new ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the street
     *
     * @return string $street
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Sets the street
     *
     * @param string $street
     * @return void
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * Returns the zip
     *
     * @return string $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param string $zip
     * @return void
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the configurationMap
     *
     * @return string $configurationMap
     */
    public function getConfigurationMap()
    {
        return $this->configurationMap;
    }

    /**
     * Sets the configurationMap
     *
     * @param string $configurationMap
     * @return void
     */
    public function setConfigurationMap($configurationMap)
    {
        $this->configurationMap = $configurationMap;
    }

    /**
     * Returns the latitude
     *
     * @return float $latitude
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Sets the latitude
     *
     * @param float $latitude
     * @return void
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * Returns the longitude
     *
     * @return float $longitude
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Sets the longitude
     *
     * @param float $longitude
     * @return void
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * Returns the address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the address
     *
     * @param string $address
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Returns the marker
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $marker
     */
    public function getMarker()
    {
        return $this->marker;
    }

    /**
     * Sets the marker
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $marker
     * @return void
     */
    public function setMarker(\TYPO3\CMS\Extbase\Domain\Model\FileReference $marker)
    {
        $this->marker = $marker;
    }

    /**
     * Returns the imageSize
     *
     * @return bool $imageSize
     */
    public function getImageSize()
    {
        return $this->imageSize;
    }

    /**
     * Sets the imageSize
     *
     * @param bool $imageSize
     * @return void
     */
    public function setImageSize($imageSize)
    {
        $this->imageSize = $imageSize;
    }

    /**
     * Returns the boolean state of imageSize
     *
     * @return bool
     */
    public function isImageSize()
    {
        return $this->imageSize;
    }

    /**
     * Returns the imageWidth
     *
     * @return int $imageWidth
     */
    public function getImageWidth()
    {
        return $this->imageWidth;
    }

    /**
     * Sets the imageWidth
     *
     * @param int $imageWidth
     * @return void
     */
    public function setImageWidth($imageWidth)
    {
        $this->imageWidth = $imageWidth;
    }

    /**
     * Returns the imageHeight
     *
     * @return int $imageHeight
     */
    public function getImageHeight()
    {
        return $this->imageHeight;
    }

    /**
     * Sets the imageHeight
     *
     * @param int $imageHeight
     * @return void
     */
    public function setImageHeight($imageHeight)
    {
        $this->imageHeight = $imageHeight;
    }

    /**
     * Returns the MarkerAnchorX
     *
     * @return int $markerAnchorX
     */
    public function getMarkerAnchorX()
    {
        return $this->markerAnchorX;
    }

    /**
     * Sets the MarkerAnchorX
     *
     * @param int $markerAnchorX
     * @return void
     */
    public function setMarkerAnchorX($markerAnchorX)
    {
        $this->markerAnchorX = $markerAnchorX;
    }

    /**
     * Returns the MarkerAnchorY
     *
     * @return int $markerAnchorY
     */
    public function getMarkerAnchorY()
    {
        return $this->markerAnchorY;
    }

    /**
     * Sets the MarkerAnchorY
     *
     * @param int $markerAnchorY
     * @return void
     */
    public function setMarkerAnchorY($markerAnchorY)
    {
        $this->markerAnchorY = $markerAnchorY;
    }

    /**
     * Returns the PopupAnchorX
     *
     * @return int $popupAnchorX
     */
    public function getPopupAnchorX()
    {
        return $this->popupAnchorX;
    }

    /**
     * Sets the PopupAnchorX
     *
     * @param int $popupAnchorX
     * @return void
     */
    public function setPopupAnchorX($popupAnchorX)
    {
        $this->popupAnchorX = $popupAnchorX;
    }

    /**
     * Returns the PopupAnchorY
     *
     * @return int $popupAnchorY
     */
    public function getPopupAnchorY()
    {
        return $this->popupAnchorY;
    }

    /**
     * Sets thePopupAnchorY
     *
     * @param int $popupAnchorY
     * @return void
     */
    public function setPopupAnchorY($popupAnchorY)
    {
        $this->popupAnchorY = $popupAnchorY;
    }

    /**
     * Returns the infoWindowContent
     *
     * @return string $infoWindowContent
     */
    public function getInfoWindowContent()
    {
        return $this->infoWindowContent;
    }

    /**
     * Sets the infoWindowContent
     *
     * @param string $infoWindowContent
     * @return void
     */
    public function setInfoWindowsContent($infoWindowContent)
    {
        $this->infoWindowContent = $infoWindowContent;
    }

    /**
     * Adds a FileReference
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $infoWindowImage
     * @return void
     */
    public function addInfoWindowImage(FileReference $infoWindowImage)
    {
        $this->infoWindowImages->attach($infoWindowImage);
    }

    /**
     * Removes a FileReference
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $infoWindowImageToRemove The FileReference to be removed
     * @return void
     */
    public function removeInfoWindowImage(FileReference $infoWindowImageToRemove)
    {
        $this->infoWindowImages->detach($infoWindowImageToRemove);
    }

    /**
     * Returns the infoWindowImages
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $infoWindowImages
     */
    public function getInfoWindowImages()
    {
        return $this->infoWindowImages;
    }

    /**
     * Sets the infoWindowImages
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $infoWindowImages
     * @return void
     */
    public function setInfoWindowImage(ObjectStorage $infoWindowImages)
    {
        $this->infoWindowImages = $infoWindowImages;
    }

    /**
     * Returns the closeByClick
     *
     * @return bool $closeByClick
     */
    public function getCloseByClick()
    {
        return $this->closeByClick;
    }

    /**
     * Sets the closeByClick
     *
     * @param bool $closeByClick
     * @return void
     */
    public function setCloseByClick($closeByClick)
    {
        $this->closeByClick = $closeByClick;
    }

    /**
     * Returns the boolean state of closeByClick
     *
     * @return bool
     */
    public function isCloseByClick()
    {
        return $this->closeByClick;
    }

    /**
     * Returns the openByClick
     *
     * @return bool $openByClick
     */
    public function getOpenByClick()
    {
        return $this->openByClick;
    }

    /**
     * Sets the openByClick
     *
     * @param bool $openByClick
     * @return void
     */
    public function setOpenByClick($openByClick)
    {
        $this->openByClick = $openByClick;
    }

    /**
     * Returns the boolean state of openByClick
     *
     * @return bool
     */
    public function isOpenByClick()
    {
        return $this->openByClick;
    }

    /**
     * Returns the opened
     *
     * @return bool $opened
     */
    public function getOpened()
    {
        return $this->opened;
    }

    /**
     * Sets the opened
     *
     * @param bool $opened
     * @return void
     */
    public function setOpened($opened)
    {
        $this->opened = $opened;
    }

    /**
     * Returns the boolean state of opened
     *
     * @return bool
     */
    public function isOpened()
    {
        return $this->opened;
    }

    /**
     * Adds a Category
     *
     * @param \Typo3graf\LeafletOsm\Domain\Model\Category $category
     * @return void
     */
    public function addAddress(Category $category)
    {
        $this->addresses->attach($category);
    }

    /**
     * Removes a Category
     *
     * @param \Typo3graf\LeafletOsm\Domain\Model\Category $categoryToRemove The Category to be removed
     * @return void
     */
    public function removeAddress(Category $categoryToRemove)
    {
        $this->addresses->detach($categoryToRemove);
    }

    /**
     * Returns the Categories
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Typo3graf\LeafletOsm\Domain\Model\Category> $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Sets the Categories
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Typo3graf\LeafletOsm\Domain\Model\Category> $categories
     * @return void
     */
    public function setCategories(ObjectStorage $categories)
    {
        $this->categories = $categories;
    }
}
