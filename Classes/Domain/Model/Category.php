<?php

namespace Typo3graf\LeafletOsm\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Domain\Model\FileReference;

class Category extends \TYPO3\CMS\Extbase\Domain\Model\Category
{
    /**
     * marker
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $osmMarker;
    /**
     * imageSize
     *
     * @var boolean
     */
    protected $osmImageSize = false;
    /**
     * imageWidth
     *
     * @var int
     */
    protected $osmImageWidth;
    /**
     * imageHeight
     *
     * @var int
     */
    protected $osmImageHeight;

    /**
     * markerAnchorX
     *
     * @var int
     */
    protected $osmMarkerAnchorX = 0;

    /**
     * markerAnchorY
     *
     * @var int
     */
    protected $osmMarkerAnchorY = 0;

    /**
     * popupAnchorX
     *
     * @var int
     */
    protected $osmPopupAnchorX = 0;

    /**
     * popupAnchorY
     *
     * @var int
     */
    protected $osmPopupAnchorY = 0;

    /**
     * Returns the marker
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference osmMarker
     */
    public function getOsmMarker()
    {
        return $this->osmMarker;
    }

    /**
     * Sets the osmMarker
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $osmMarker
     * @return void
     */
    public function setOsmMarker(FileReference $osmMarker)
    {
        $this->osmMarker = $osmMarker;
    }

    /**
     * Returns the imageSize
     *
     * @return boolean $osmImageSize
     */
    public function getOsmImageSize()
    {
        return $this->osmImageSize;
    }

    /**
     * Returns the boolean state of imageSize
     *
     * @return boolean
     */
    public function isOsmImageSize()
    {
        return $this->getOsmImageSize();
    }

    /**
     * Sets the imageSize
     *
     * @param boolean $osmImageSize
     * @return void
     */
    public function setOsmImageSize($osmImageSize)
    {
        $this->osmImageSize = $osmImageSize;
    }

    /**
     * @return int
     */
    public function getOsmImageWidth()
    {
        return $this->osmImageWidth;
    }

    /**
     * @param int $osmImageWidth
     */
    public function setOsmImageWidth($osmImageWidth)
    {
        $this->osmImageWidth = $osmImageWidth;
    }

    /**
     * @return int
     */
    public function getOsmImageHeight()
    {
        return $this->osmImageHeight;
    }

    /**
     * @param int $osmImageHeight
     */
    public function setOsmImageHeight($osmImageHeight)
    {
        $this->osmImageHeight = $osmImageHeight;
    }

    /**
     * Returns the MarkerAnchorX
     *
     * @return int $osmMarkerAnchorX
     */
    public function getOsmMarkerAnchorX()
    {
        return $this->osmMarkerAnchorX;
    }

    /**
     * Sets the MarkerAnchorX
     *
     * @param int $osmMarkerAnchorX
     * @return void
     */
    public function setOsmMarkerAnchorX($osmMarkerAnchorX)
    {
        $this->osmMarkerAnchorX = $osmMarkerAnchorX;
    }

    /**
     * Returns the MarkerAnchorY
     *
     * @return int $osmMarkerAnchorY
     */
    public function getOsmMarkerAnchorY()
    {
        return $this->osmMarkerAnchorY;
    }

    /**
     * Sets the MarkerAnchorY
     *
     * @param int $osmMarkerAnchorY
     * @return void
     */
    public function setOsmMarkerAnchorY($osmMarkerAnchorY)
    {
        $this->osmMarkerAnchorY = $osmMarkerAnchorY;
    }

    /**
     * Returns the PopupAnchorX
     *
     * @return int $osmPopupAnchorX
     */
    public function getOsmPopupAnchorX()
    {
        return $this->osmPopupAnchorX;
    }

    /**
     * Sets the PopupAnchorX
     *
     * @param int $osmPopupAnchorX
     * @return void
     */
    public function setOsmPopupAnchorX($osmPopupAnchorX)
    {
        $this->osmPopupAnchorX = $osmPopupAnchorX;
    }

    /**
     * Returns the PopupAnchorY
     *
     * @return int $osmPopupAnchorY
     */
    public function getOsmPopupAnchorY()
    {
        return $this->osmPopupAnchorY;
    }

    /**
     * Sets thePopupAnchorY
     *
     * @param int $osmPopupAnchorY
     * @return void
     */
    public function setPopupAnchorY($osmPopupAnchorY)
    {
        $this->osmPopupAnchorY = $osmPopupAnchorY;
    }

}

