<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('leaflet_osm', 'Configuration/TypoScript', 'Leaflet-OpenStreetMaps');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_leafletosm_domain_model_address', 'EXT:leaflet_osm/Resources/Private/Language/locallang_csh_tx_leafletosm_domain_model_address.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_leafletosm_domain_model_address');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_leafletosm_domain_model_map', 'EXT:leaflet_osm/Resources/Private/Language/locallang_csh_tx_leafletosm_domain_model_map.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_leafletosm_domain_model_map');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_leafletosm_domain_model_category', 'EXT:leaflet_osm/Resources/Private/Language/locallang_csh_tx_leafletosm_domain_model_category.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_leafletosm_domain_model_category');

    }
);

/**
 * Add own CSS for Backend
*/
$GLOBALS['TBE_STYLES']['skins']['leaflet_osm']['stylesheetDirectories'][] = 'EXT:leaflet_osm/Resources/Public/Contrib/leaflet/';

