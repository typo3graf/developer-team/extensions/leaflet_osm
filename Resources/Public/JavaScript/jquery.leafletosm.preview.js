/**
 *  add listener to load map
 */
jQuery(document).ready(function ($) {
	var $links = $('.js-osm-show'),
		$preview = $('.js-osm-preview'),
		$mapContainer = $('.js-osm-container'),
		useCookies = $preview.data('cookie'),
		hideMap = true;

	if(useCookies) {
		if($.cookie('tx_leafletosm_show_map')) {
			$mapContainer.showOsmMap();
			showMap();
			hideMap = false;
		}
	}

	if(hideMap) {
		$mapContainer.hide();

		$links.click(function (e) {
			e.preventDefault();
			$mapContainer.showOsmMap();
			showMap();
			if(useCookies) {
				$.cookie('tx_leafletosm_show_map', 1, {path:'/'});
			}
		});
	}

	function showMap() {

			$mapContainer.show();

		$preview.remove();
	}

});
