<?php
namespace Typo3graf\LeafletOsm\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Typo3graf Developer-Team <development@typo3graf.de>
 */
class MapTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Typo3graf\LeafletOsm\Domain\Model\Map
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Typo3graf\LeafletOsm\Domain\Model\Map();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getWidthReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getWidth()
        );
    }

    /**
     * @test
     */
    public function setWidthForStringSetsWidth()
    {
        $this->subject->setWidth('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'width',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getHeightReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getHeight()
        );
    }

    /**
     * @test
     */
    public function setHeightForStringSetsHeight()
    {
        $this->subject->setHeight('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'height',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getZoomReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getZoom()
        );
    }

    /**
     * @test
     */
    public function setZoomForIntSetsZoom()
    {
        $this->subject->setZoom(12);

        self::assertAttributeEquals(
            12,
            'zoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getShowAddressesReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getShowAddresses()
        );
    }

    /**
     * @test
     */
    public function setShowAddressesForBoolSetsShowAddresses()
    {
        $this->subject->setShowAddresses(true);

        self::assertAttributeEquals(
            true,
            'showAddresses',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getShoeCategoriesReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getShoeCategories()
        );
    }

    /**
     * @test
     */
    public function setShoeCategoriesForBoolSetsShoeCategories()
    {
        $this->subject->setShoeCategories(true);

        self::assertAttributeEquals(
            true,
            'shoeCategories',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getKmlUrlReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getKmlUrl()
        );
    }

    /**
     * @test
     */
    public function setKmlUrlForStringSetsKmlUrl()
    {
        $this->subject->setKmlUrl('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'kmlUrl',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getKmlPreserveViewportReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getKmlPreserveViewport()
        );
    }

    /**
     * @test
     */
    public function setKmlPreserveViewportForBoolSetsKmlPreserveViewport()
    {
        $this->subject->setKmlPreserveViewport(true);

        self::assertAttributeEquals(
            true,
            'kmlPreserveViewport',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getKmlLocalReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getKmlLocal()
        );
    }

    /**
     * @test
     */
    public function setKmlLocalForBoolSetsKmlLocal()
    {
        $this->subject->setKmlLocal(true);

        self::assertAttributeEquals(
            true,
            'kmlLocal',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getScrollZoomReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getScrollZoom()
        );
    }

    /**
     * @test
     */
    public function setScrollZoomForBoolSetsScrollZoom()
    {
        $this->subject->setScrollZoom(true);

        self::assertAttributeEquals(
            true,
            'scrollZoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMinZoomReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getMinZoom()
        );
    }

    /**
     * @test
     */
    public function setMinZoomForIntSetsMinZoom()
    {
        $this->subject->setMinZoom(12);

        self::assertAttributeEquals(
            12,
            'minZoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMaxZoomReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMaxZoom()
        );
    }

    /**
     * @test
     */
    public function setMaxZoomForStringSetsMaxZoom()
    {
        $this->subject->setMaxZoom('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'maxZoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCenterLongitudeReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getCenterLongitude()
        );
    }

    /**
     * @test
     */
    public function setCenterLongitudeForFloatSetsCenterLongitude()
    {
        $this->subject->setCenterLongitude(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'centerLongitude',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getCenterLatitudeReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getCenterLatitude()
        );
    }

    /**
     * @test
     */
    public function setCenterLatitudeForFloatSetsCenterLatitude()
    {
        $this->subject->setCenterLatitude(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'centerLatitude',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getGeoLocationReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getGeoLocation()
        );
    }

    /**
     * @test
     */
    public function setGeoLocationForBoolSetsGeoLocation()
    {
        $this->subject->setGeoLocation(true);

        self::assertAttributeEquals(
            true,
            'geoLocation',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPreviewImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getPreviewImage()
        );
    }

    /**
     * @test
     */
    public function setPreviewImageForFileReferenceSetsPreviewImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setPreviewImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'previewImage',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDraggableReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getDraggable()
        );
    }

    /**
     * @test
     */
    public function setDraggableForBoolSetsDraggable()
    {
        $this->subject->setDraggable(true);

        self::assertAttributeEquals(
            true,
            'draggable',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDoubleClickZoomReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getDoubleClickZoom()
        );
    }

    /**
     * @test
     */
    public function setDoubleClickZoomForBoolSetsDoubleClickZoom()
    {
        $this->subject->setDoubleClickZoom(true);

        self::assertAttributeEquals(
            true,
            'doubleClickZoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMarkerClusterReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getMarkerCluster()
        );
    }

    /**
     * @test
     */
    public function setMarkerClusterForBoolSetsMarkerCluster()
    {
        $this->subject->setMarkerCluster(true);

        self::assertAttributeEquals(
            true,
            'markerCluster',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMarkerClusterZoomReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getMarkerClusterZoom()
        );
    }

    /**
     * @test
     */
    public function setMarkerClusterZoomForIntSetsMarkerClusterZoom()
    {
        $this->subject->setMarkerClusterZoom(12);

        self::assertAttributeEquals(
            12,
            'markerClusterZoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMarkerClusterSizeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getMarkerClusterSize()
        );
    }

    /**
     * @test
     */
    public function setMarkerClusterSizeForIntSetsMarkerClusterSize()
    {
        $this->subject->setMarkerClusterSize(12);

        self::assertAttributeEquals(
            12,
            'markerClusterSize',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMarkerSearchReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getMarkerSearch()
        );
    }

    /**
     * @test
     */
    public function setMarkerSearchForBoolSetsMarkerSearch()
    {
        $this->subject->setMarkerSearch(true);

        self::assertAttributeEquals(
            true,
            'markerSearch',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDefaultTypeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getDefaultType()
        );
    }

    /**
     * @test
     */
    public function setDefaultTypeForIntSetsDefaultType()
    {
        $this->subject->setDefaultType(12);

        self::assertAttributeEquals(
            12,
            'defaultType',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getScaleControlReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getScaleControl()
        );
    }

    /**
     * @test
     */
    public function setScaleControlForBoolSetsScaleControl()
    {
        $this->subject->setScaleControl(true);

        self::assertAttributeEquals(
            true,
            'scaleControl',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFullscreenControlReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getFullscreenControl()
        );
    }

    /**
     * @test
     */
    public function setFullscreenControlForBoolSetsFullscreenControl()
    {
        $this->subject->setFullscreenControl(true);

        self::assertAttributeEquals(
            true,
            'fullscreenControl',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getZoomControlReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getZoomControl()
        );
    }

    /**
     * @test
     */
    public function setZoomControlForBoolSetsZoomControl()
    {
        $this->subject->setZoomControl(true);

        self::assertAttributeEquals(
            true,
            'zoomControl',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMapTypeControlReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getMapTypeControl()
        );
    }

    /**
     * @test
     */
    public function setMapTypeControlForBoolSetsMapTypeControl()
    {
        $this->subject->setMapTypeControl(true);

        self::assertAttributeEquals(
            true,
            'mapTypeControl',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMapTypesReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMapTypes()
        );
    }

    /**
     * @test
     */
    public function setMapTypesForStringSetsMapTypes()
    {
        $this->subject->setMapTypes('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'mapTypes',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getShowRouteReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getShowRoute()
        );
    }

    /**
     * @test
     */
    public function setShowRouteForBoolSetsShowRoute()
    {
        $this->subject->setShowRoute(true);

        self::assertAttributeEquals(
            true,
            'showRoute',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCalcRouteReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getCalcRoute()
        );
    }

    /**
     * @test
     */
    public function setCalcRouteForBoolSetsCalcRoute()
    {
        $this->subject->setCalcRoute(true);

        self::assertAttributeEquals(
            true,
            'calcRoute',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTravelModeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTravelMode()
        );
    }

    /**
     * @test
     */
    public function setTravelModeForStringSetsTravelMode()
    {
        $this->subject->setTravelMode('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'travelMode',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMeasureSystemReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMeasureSystem()
        );
    }

    /**
     * @test
     */
    public function setMeasureSystemForStringSetsMeasureSystem()
    {
        $this->subject->setMeasureSystem('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'measureSystem',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMapAddressesReturnsInitialValueForAddress()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getMapAddresses()
        );
    }

    /**
     * @test
     */
    public function setMapAddressesForObjectStorageContainingAddressSetsMapAddresses()
    {
        $mapAddress = new \Typo3graf\LeafletOsm\Domain\Model\Address();
        $objectStorageHoldingExactlyOneMapAddresses = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneMapAddresses->attach($mapAddress);
        $this->subject->setMapAddresses($objectStorageHoldingExactlyOneMapAddresses);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneMapAddresses,
            'mapAddresses',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addMapAddressToObjectStorageHoldingMapAddresses()
    {
        $mapAddress = new \Typo3graf\LeafletOsm\Domain\Model\Address();
        $mapAddressesObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $mapAddressesObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($mapAddress));
        $this->inject($this->subject, 'mapAddresses', $mapAddressesObjectStorageMock);

        $this->subject->addMapAddress($mapAddress);
    }

    /**
     * @test
     */
    public function removeMapAddressFromObjectStorageHoldingMapAddresses()
    {
        $mapAddress = new \Typo3graf\LeafletOsm\Domain\Model\Address();
        $mapAddressesObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $mapAddressesObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($mapAddress));
        $this->inject($this->subject, 'mapAddresses', $mapAddressesObjectStorageMock);

        $this->subject->removeMapAddress($mapAddress);
    }
}
