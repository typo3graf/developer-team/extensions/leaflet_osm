<?php
namespace Typo3graf\LeafletOsm\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Typo3graf Developer-Team <development@typo3graf.de>
 */
class CategoryTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Typo3graf\LeafletOsm\Domain\Model\Category
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Typo3graf\LeafletOsm\Domain\Model\Category();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getLosmMarkerReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getLosmMarker()
        );
    }

    /**
     * @test
     */
    public function setLosmMarkerForFileReferenceSetsLosmMarker()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setLosmMarker($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'losmMarker',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLosmImageSizeReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getLosmImageSize()
        );
    }

    /**
     * @test
     */
    public function setLosmImageSizeForBoolSetsLosmImageSize()
    {
        $this->subject->setLosmImageSize(true);

        self::assertAttributeEquals(
            true,
            'losmImageSize',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLosmImageWidthReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getLosmImageWidth()
        );
    }

    /**
     * @test
     */
    public function setLosmImageWidthForIntSetsLosmImageWidth()
    {
        $this->subject->setLosmImageWidth(12);

        self::assertAttributeEquals(
            12,
            'losmImageWidth',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLosmImageHeightReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getLosmImageHeight()
        );
    }

    /**
     * @test
     */
    public function setLosmImageHeightForIntSetsLosmImageHeight()
    {
        $this->subject->setLosmImageHeight(12);

        self::assertAttributeEquals(
            12,
            'losmImageHeight',
            $this->subject
        );
    }
}
