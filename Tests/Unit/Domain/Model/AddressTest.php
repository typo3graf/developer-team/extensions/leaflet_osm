<?php
namespace Typo3graf\LeafletOsm\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Typo3graf Developer-Team <development@typo3graf.de>
 */
class AddressTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Typo3graf\LeafletOsm\Domain\Model\Address
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Typo3graf\LeafletOsm\Domain\Model\Address();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStreetReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getStreet()
        );
    }

    /**
     * @test
     */
    public function setStreetForStringSetsStreet()
    {
        $this->subject->setStreet('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'street',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getZipReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getZip()
        );
    }

    /**
     * @test
     */
    public function setZipForStringSetsZip()
    {
        $this->subject->setZip('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'zip',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCityReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCity()
        );
    }

    /**
     * @test
     */
    public function setCityForStringSetsCity()
    {
        $this->subject->setCity('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'city',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getConfigurationMapReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getConfigurationMap()
        );
    }

    /**
     * @test
     */
    public function setConfigurationMapForStringSetsConfigurationMap()
    {
        $this->subject->setConfigurationMap('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'configurationMap',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLatitudeReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getLatitude()
        );
    }

    /**
     * @test
     */
    public function setLatitudeForFloatSetsLatitude()
    {
        $this->subject->setLatitude(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'latitude',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getLongitudeReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getLongitude()
        );
    }

    /**
     * @test
     */
    public function setLongitudeForFloatSetsLongitude()
    {
        $this->subject->setLongitude(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'longitude',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getAddressReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAddress()
        );
    }

    /**
     * @test
     */
    public function setAddressForStringSetsAddress()
    {
        $this->subject->setAddress('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'address',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMarkerReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getMarker()
        );
    }

    /**
     * @test
     */
    public function setMarkerForFileReferenceSetsMarker()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setMarker($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'marker',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageSizeReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getImageSize()
        );
    }

    /**
     * @test
     */
    public function setImageSizeForBoolSetsImageSize()
    {
        $this->subject->setImageSize(true);

        self::assertAttributeEquals(
            true,
            'imageSize',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageWidthReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getImageWidth()
        );
    }

    /**
     * @test
     */
    public function setImageWidthForIntSetsImageWidth()
    {
        $this->subject->setImageWidth(12);

        self::assertAttributeEquals(
            12,
            'imageWidth',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageHeightReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getImageHeight()
        );
    }

    /**
     * @test
     */
    public function setImageHeightForIntSetsImageHeight()
    {
        $this->subject->setImageHeight(12);

        self::assertAttributeEquals(
            12,
            'imageHeight',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInfoWindowsContentReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInfoWindowsContent()
        );
    }

    /**
     * @test
     */
    public function setInfoWindowsContentForStringSetsInfoWindowsContent()
    {
        $this->subject->setInfoWindowsContent('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'infoWindowsContent',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInfoWindowsImagesReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getInfoWindowsImages()
        );
    }

    /**
     * @test
     */
    public function setInfoWindowsImagesForFileReferenceSetsInfoWindowsImages()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setInfoWindowsImages($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'infoWindowsImages',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCloseByClickReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getCloseByClick()
        );
    }

    /**
     * @test
     */
    public function setCloseByClickForBoolSetsCloseByClick()
    {
        $this->subject->setCloseByClick(true);

        self::assertAttributeEquals(
            true,
            'closeByClick',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getOpenByClickReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getOpenByClick()
        );
    }

    /**
     * @test
     */
    public function setOpenByClickForBoolSetsOpenByClick()
    {
        $this->subject->setOpenByClick(true);

        self::assertAttributeEquals(
            true,
            'openByClick',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getOpenedReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getOpened()
        );
    }

    /**
     * @test
     */
    public function setOpenedForBoolSetsOpened()
    {
        $this->subject->setOpened(true);

        self::assertAttributeEquals(
            true,
            'opened',
            $this->subject
        );
    }
}
